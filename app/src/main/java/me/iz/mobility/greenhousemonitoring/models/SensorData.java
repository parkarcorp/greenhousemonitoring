/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 2/24/16 12:41 AM
 *  @modified 2/24/16 12:41 AM
 */

package me.iz.mobility.greenhousemonitoring.models;

/**
 * @author ibasit
 */
public class SensorData {

    private final String TAG = getClass().getSimpleName();

    public String moisture;

    public String humidity;

    public String temperature;

    public String light;

    public SensorData(String moisture, String humidity, String temperature, String light) {
        this.moisture = moisture;
        this.humidity = humidity;
        this.temperature = temperature;
        this.light = light;
    }
}
