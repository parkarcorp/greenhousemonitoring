/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 2/24/16 12:04 AM
 *  @modified 2/23/16 10:15 PM
 */

package me.iz.mobility.greenhousemonitoring;

import android.app.Application;

import me.iz.mobility.greenhousemonitoring.utils.Preferences;
import timber.log.Timber;

/**
 * @author ibasit
 */
public class GreenHouseMonitor extends Application {

    private final String TAG = getClass().getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();

        Preferences.init(getApplicationContext());

        if(BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
