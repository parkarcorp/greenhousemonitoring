/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 2/24/16 12:08 AM
 *  @modified 2/23/16 11:53 PM
 */

package me.iz.mobility.greenhousemonitoring;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import butterknife.Bind;
import butterknife.OnClick;
import me.iz.mobility.greenhousemonitoring.models.SensorData;
import me.iz.mobility.greenhousemonitoring.utils.Constants;
import me.iz.mobility.greenhousemonitoring.utils.Preferences;
import timber.log.Timber;

public class GreenHouseMonitorActivity extends BaseBluetoothActivity {

    private static final int ON = 1;
    private static final int OFF = 0;


    @Bind(R.id.tbHumidity)
    ToggleButton tbHumidity;

    @Bind(R.id.tvHumidity)
    TextView tvHumidity;

    @Bind(R.id.tbMoisture)
    ToggleButton tbMoisture;

    @Bind(R.id.tvMoisture)
    TextView tvMoisture;

    @Bind(R.id.tbSoil)
    ToggleButton tbTemperature;

    @Bind(R.id.tvTemperature)
    TextView tvTemperature;

    @Bind(R.id.tbLight)
    ToggleButton tbLight;

    @Bind(R.id.tvLight)
    TextView tvLight;

    @Bind(R.id.btnConnect)
    Button btnConnect;

    @Bind(R.id.tbRequest)
    ToggleButton tbRequest;

    @Bind(R.id.btnSend)
    Button btnSend;

    private SensorStatus sensorStatus;

    private int moistureThreshold;
    private int temperatureThreshold;
    private int humidityThreshold;
    private int lightThreshold;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_green_house_monitor);

        sensorStatus = new SensorStatus();

        toggleListeners();

        connectionCallbacks();

        dataReceiver();

        humidityThreshold = Preferences.getInteger(Constants.HUMIDITY);
        temperatureThreshold = Preferences.getInteger(Constants.TEMPERATURE);
        lightThreshold = Preferences.getInteger(Constants.LIGHT);
        moistureThreshold = Preferences.getInteger(Constants.MOISTURE);

    }

    /*
     * Toggle button listeners
     */
    private void toggleListeners() {

        tbMoisture.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    sensorStatus.moisture = ON;
                else
                    sensorStatus.moisture = OFF;
            }
        });

        tbHumidity.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    sensorStatus.humidity = ON;
                else
                    sensorStatus.humidity = OFF;
            }
        });

        tbTemperature.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    sensorStatus.soil = ON;
                else
                    sensorStatus.soil = OFF;
            }
        });

        tbLight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    sensorStatus.light = ON;
                else
                    sensorStatus.light = OFF;
            }
        });

        tbRequest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    sensorStatus.reqStatus = ON;
                    btnSend.setEnabled(true);

                    disableToggle(tbHumidity);
                    disableToggle(tbLight);
                    disableToggle(tbMoisture);
                    disableToggle(tbTemperature);


                } else {
                    sensorStatus.reqStatus = OFF;
                    btnSend.setEnabled(false);

                    enableToggle(tbHumidity);
                    enableToggle(tbLight);
                    enableToggle(tbMoisture);
                    enableToggle(tbTemperature);
                }

            }
        });

    }

    /**
     * Method to set values obtained from sensor
     * @param sensorData
     */
    private void setValues(SensorData sensorData) {
        tvMoisture.setText(sensorData.moisture);
        tvHumidity.setText(sensorData.humidity);
        tvTemperature.setText(sensorData.temperature);
        tvLight.setText(sensorData.light);

        /* Execute only when in auto mode*/
        if (tbRequest.isChecked()) {

            boolean check = thresholdCheck(temperatureThreshold,
                    Integer.parseInt(sensorData.temperature));
            Timber.d("Temperature %s -- threshold %s -- value %s", check,
                    temperatureThreshold, sensorData.temperature);
            tbTemperature.setChecked(check);

            check = thresholdCheck(humidityThreshold,
                    Integer.parseInt(sensorData.humidity));
            Timber.d("Humidity %s -- threshold %s -- value %s", check,
                    humidityThreshold, sensorData.humidity);
            tbHumidity.setChecked(check);

            check = thresholdCheck(moistureThreshold,
                    Integer.parseInt(sensorData.moisture));
            Timber.d("Moisture %s -- threshold %s -- value %s", check,
                    moistureThreshold, sensorData.moisture);
            tbMoisture.setChecked(check);

            check = thresholdCheck(lightThreshold,
                    Integer.parseInt(sensorData.light));
            Timber.d("Light %s -- threshold %s -- value %s", check,
                    lightThreshold, sensorData.light);
            tbLight.setChecked(check);


            sendSensorStatus();
        }


    }

    /**
     * Parse the data obtained from bluetooth
     * @param response
     * @return
     */
    private SensorData getSensorData(String response) {
        String arr[] = response.split(",");
        return new SensorData(arr[0], arr[1], arr[2], arr[3]);
    }

    private void dataReceiver() {

        btHandle.setOnDataReceivedListener(new BluetoothSPP.OnDataReceivedListener() {
            @Override
            public void onDataReceived(byte[] data, String message) {
                Timber.d("Data received %s", message);

                setValues(getSensorData(message));

            }
        });
    }

    /**
     * Connection callback for bluetooth
     */
    private void connectionCallbacks() {
        btHandle.setBluetoothConnectionListener(new BluetoothSPP.BluetoothConnectionListener() {
            @Override
            public void onDeviceConnected(String name, String address) {
                Timber.d("Connected to %s : %s", name, address);
                btnConnect.setVisibility(View.GONE);

                if (tbRequest.isChecked()) {
                    sendSensorStatus();
                }
            }

            @Override
            public void onDeviceDisconnected() {
                Timber.w("Device disconnected!!");

                Toast.makeText(getApplicationContext()
                        , "Bluetooth disconnected"
                        , Toast.LENGTH_SHORT).show();

                btnConnect.setVisibility(View.VISIBLE);
            }

            @Override
            public void onDeviceConnectionFailed() {
                Timber.d("Device connection failed");
                Toast.makeText(getApplicationContext()
                        , "Bluetooth connection failed"
                        , Toast.LENGTH_SHORT).show();

            }
        });
    }

    /**
     * Method to send status
     */
    @OnClick(R.id.btnSend)
    public void onClickSend() {
        sendSensorStatus();
    }

    /**
     * Method to send bluetooth request
     */
    private void sendSensorStatus() {
        String sensorStat = sensorStatus.toString();
        Timber.d("Sending %s", sensorStat);
        send(sensorStat);
    }


    private void enableToggle(ToggleButton tb) {
        tb.setEnabled(true);
    }

    private void disableToggle(ToggleButton tb) {
        tb.setEnabled(false);
    }

    /**
     * Button click listener for settings
     */
    @OnClick(R.id.btnConfig)
    public void settings() {
        Intent intent = new Intent(this, ConfigurationActivity.class);
        startActivity(intent);
    }


    /**
     * Method to check if the current value has crossed the threshold
     *
     * @param threshold
     * @param currentValue
     * @return
     */
    private boolean thresholdCheck(int threshold, int currentValue) {
        return currentValue >= threshold;
    }


}
