/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 3/17/16 10:37 AM
 *  @modified 3/17/16 10:35 AM
 */

package me.iz.mobility.greenhousemonitoring;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.iz.mobility.greenhousemonitoring.utils.Constants;
import me.iz.mobility.greenhousemonitoring.utils.Preferences;

public class ConfigurationActivity extends AppCompatActivity {

    @Bind(R.id.etHumidity)
    EditText etHumidity;

    @Bind(R.id.etLight)
    EditText etLight;

    @Bind(R.id.etMoisture)
    EditText etMoisture;

    @Bind(R.id.etTemperature)
    EditText etTemperature;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration);
        ButterKnife.bind(this);

        etHumidity.setText(Preferences.getInteger(Constants.HUMIDITY)+"");
        etTemperature.setText(Preferences.getInteger(Constants.TEMPERATURE)+"");
        etLight.setText(Preferences.getInteger(Constants.LIGHT)+"");
        etMoisture.setText(Preferences.getInteger(Constants.MOISTURE)+"");

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }


    @OnClick(R.id.btnSave)
    public void onSaveClicked() {

        if (validate()) {
            Toast.makeText(ConfigurationActivity.this, "Configuration saved successfully", Toast.LENGTH_SHORT).show();
            finish();
        }

    }

    private boolean validate() {

        String humidity = etHumidity.getText().toString();

        String light = etLight.getText().toString();

        String moisture = etMoisture.getText().toString();

        String temperature = etTemperature.getText().toString();

        if (humidity.isEmpty()) {
            etHumidity.setError("Please enter a valid threshold humidity ");
            return false;
        }

        if (light.isEmpty()) {
            etLight.setError("Please enter a valid threshold for light ");
            return false;
        }

        if (moisture.isEmpty()) {
            etMoisture.setError("Please enter a valid threshold for moisture ");
            return false;
        }
        if (temperature.isEmpty()) {
            etTemperature.setError("Please enter a valid threshold for temperature ");
            return false;
        }

        Preferences.saveInteger(Constants.TEMPERATURE, Integer.parseInt(temperature));
        Preferences.saveInteger(Constants.HUMIDITY, Integer.parseInt(humidity));
        Preferences.saveInteger(Constants.MOISTURE, Integer.parseInt(moisture));
        Preferences.saveInteger(Constants.LIGHT, Integer.parseInt(light));

        return true;

    }
}
