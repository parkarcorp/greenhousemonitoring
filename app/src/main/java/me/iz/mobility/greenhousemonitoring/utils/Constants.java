/*
 * Copyright 2016 Basit Parkar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *
 *  @date 3/17/16 10:56 AM
 *  @modified 3/17/16 10:56 AM
 */

package me.iz.mobility.greenhousemonitoring.utils;

/**
 * @author ibasit
 */
public class Constants {

    private final String TAG = getClass().getSimpleName();

    public static final String TEMPERATURE = "1111";

    public static final String HUMIDITY = "2222";

    public static final String LIGHT = "3333";

    public static final String MOISTURE = "4444";


}
